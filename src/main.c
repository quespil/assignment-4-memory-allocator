
#include "mem.h"

#include <assert.h>
#include <stdio.h>

void start_test(const char* test_name, void (*function)()){
    heap_init(0);
    printf("%s\n", test_name);
    function();
    heap_term();
    printf("Success.\n");

}
void test_successful_memory_allocation() {
    void* mem1 = _malloc(1);
    assert(mem1 != NULL);
    _free(mem1);
}

void test_single_block_free() {
    void* mem1 = _malloc(100);
    void* mem2 = _malloc(200);
    _free(mem1);
    _free(mem2);
}

void test_double_block_free() {
    void* mem1 = _malloc(64);
    void* mem2 = _malloc(128);
    _free(mem1);
    _free(mem2);
}

void test_memory_expansion() {
    void* mem1 = _malloc(100000);
    assert(mem1 != NULL);
    _free(mem1);
}

void test_memory_expansion_with_address_limitation() {
    void* mem1 = _malloc(100000);
    void* mem2 = _malloc(100000);
    assert(mem1 != NULL && mem2 != NULL);
    _free(mem1);
    void* mem3 = _malloc(200000);
    assert(mem3 != NULL);
    _free(mem2);
    _free(mem3);
}

int main() {
    start_test("TEST #1: successful_memory_allocation", test_successful_memory_allocation);
    start_test("TEST #2: single_block_free", test_single_block_free);
    start_test("TEST #3: double_block_free", test_double_block_free);
    start_test("TEST #4: memory_expansion", test_memory_expansion);
    start_test("TEST #5: memory_expansion_with_address_limitation", test_memory_expansion_with_address_limitation);
    return 0;
}
